function converterDuracao (){
    
    //cria referencia aos elementos da pagina
    var inTitulo = document.getElementById("inTitulo")
    var inDuracao = document.getElementById("inDuracao")
    var outTitulo = document.getElementById("outTitulo")
    var outRespost = document.getElementById("outRespost")

    //obtem conteudos dos campos de entrada e converte
    //a string para numero
    var titulo = inTitulo.nodeValue
    console.log(inDuracao.value)
    var duracao = Number(inDuracao.value)
    
    //arredonda para baixo o resultado da divisao
    var horas = Math.floor(duracao / 60)

    //Obtem o resto da divisao entre os numeros
    var minutos = duracao % 60
    
    //Altera o conteudo dos paragrafos de respostas e ao elmento btConverte (botao) 
    outTitulo.textContent = titulo
    outRespost.textContent = horas + " hora(s)  e " + minutos + " minutos"

  }

//Cria uma refencia ao elemento btConverter (botao)
var btConverte = document.getElementById("btConverter")

//Registra um evento associado ao botao, para carregar uma 
btConverte.addEventListener("click", converterDuracao)        